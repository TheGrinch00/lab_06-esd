LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE IEEE.NUMERIC_STD.all;

ENTITY overflow_detector IS
	PORT(A, B, S, Co : IN STD_LOGIC;
		  Ov : OUT STD_LOGIC);
END ENTITY overflow_detector;

ARCHITECTURE structural OF overflow_detector IS
BEGIN
	Ov <= ((NOT(A) AND (B XOR S)) OR (A AND (B XNOR S))) XOR Co;
END ARCHITECTURE structural;