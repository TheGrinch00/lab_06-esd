LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;

ENTITY full_adder IS 
	PORT(a, b, ci : IN STD_LOGIC;
		  s, co : OUT STD_LOGIC);
END ENTITY full_adder;

ARCHITECTURE operation OF full_adder IS 

COMPONENT mux2to1 IS
PORT(w : IN STD_LOGIC_VECTOR(0 TO 1);
	  s : IN STD_LOGIC;
	  f : OUT STD_LOGIC);
END COMPONENT mux2to1;

SIGNAL muxSel : STD_LOGIC;

BEGIN

M1 : mux2to1 PORT MAP(w(0) => b, w(1) => ci, s => muxSel, f => co);

muxSel <= a XOR b;
s <= ci XOR muxSel;
END ARCHITECTURE operation;