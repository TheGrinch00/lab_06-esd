LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

ENTITY mux2to1 IS
	PORT(w : IN STD_LOGIC_VECTOR(0 TO 1);
		  s : IN STD_LOGIC;
		  f : OUT STD_LOGIC
		  );
END ENTITY mux2to1;

ARCHITECTURE behavior OF mux2to1 IS
BEGIN
	f <= w(0) WHEN s = '0' ELSE
		  w(1);
END ARCHITECTURE behavior;