LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE IEEE.NUMERIC_STD.all;

ENTITY RCA_Adder_Subtractor_Nbit IS
	GENERIC( parallelism : INTEGER:= 8);
	PORT(A, B : IN SIGNED(parallelism-1 DOWNTO 0);
		  add_subn : IN STD_LOGIC;
		  S : BUFFER SIGNED(parallelism-1 DOWNTO 0);
		  Overflow : OUT STD_LOGIC);
END ENTITY RCA_Adder_Subtractor_Nbit;

ARCHITECTURE structural OF RCA_Adder_Subtractor_Nbit IS

COMPONENT Complementator_1 IS
	GENERIC( n : INTEGER := 8 );
	PORT( To_Be_Complemented : IN SIGNED( n-1 DOWNTO 0 );
			Activator : IN STD_LOGIC;
			Complemented : OUT SIGNED(n-1 DOWNTO 0));
END COMPONENT Complementator_1;

COMPONENT ripple_carry_adder_Nbit IS
	GENERIC(n : INTEGER := 4);
	PORT(num1, num2 : IN SIGNED(n-1 DOWNTO 0);
	  ci : IN STD_LOGIC;
	  sum : OUT SIGNED(n-1 DOWNTO 0);
	  carry_out : OUT STD_LOGIC);
END COMPONENT ripple_carry_adder_Nbit; 

COMPONENT overflow_detector IS
	PORT(A, B, S, Co : IN STD_LOGIC;
		  Ov : OUT STD_LOGIC);
END COMPONENT overflow_detector;

SIGNAL Comp_B : SIGNED(parallelism-1 DOWNTO 0);
SIGNAL carry_out : STD_LOGIC;

BEGIN

Ov_Det : overflow_detector PORT MAP(A => A(parallelism - 1), B => Comp_B(parallelism - 1), S => S(Parallelism - 1), Co => carry_out, Ov => Overflow);
C1 : Complementator_1 GENERIC MAP(n => parallelism) PORT MAP(To_Be_Complemented => B, Activator => add_subn, Complemented => Comp_B);
S1 : ripple_carry_adder_Nbit GENERIC MAP(n => parallelism) PORT MAP(num1 => A, num2 => Comp_B, sum => S, ci => add_subn, carry_out => carry_out);

END ARCHITECTURE structural;