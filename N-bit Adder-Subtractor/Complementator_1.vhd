LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE IEEE.NUMERIC_STD.all;

ENTITY Complementator_1 IS
	GENERIC( n : INTEGER := 8 );
	PORT( To_Be_Complemented : IN SIGNED( n-1 DOWNTO 0 );
			Activator : IN STD_LOGIC;
			Complemented : OUT SIGNED(n-1 DOWNTO 0));
END ENTITY Complementator_1;

ARCHITECTURE structural OF Complementator_1 IS
BEGIN
	PROCESS(To_Be_Complemented, Activator)
		BEGIN
		FOR i IN 0 TO n-1 LOOP
			Complemented(i) <= To_Be_Complemented(i) XOR Activator;
		END LOOP;
	END PROCESS;
END ARCHITECTURE structural;