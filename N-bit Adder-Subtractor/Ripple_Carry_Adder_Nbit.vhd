LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE IEEE.NUMERIC_STD.all;

ENTITY ripple_carry_adder_Nbit IS
GENERIC(n : INTEGER := 4);
PORT(num1, num2 : IN SIGNED(n-1 DOWNTO 0);
	  ci : IN STD_LOGIC;
	  sum : OUT SIGNED(n-1 DOWNTO 0);
	  carry_out : OUT STD_LOGIC);
END ENTITY ripple_carry_adder_Nbit;

ARCHITECTURE summation OF ripple_carry_adder_Nbit IS

SIGNAL CarryOuts : STD_LOGIC_VECTOR(n-2 DOWNTO 0);

COMPONENT full_adder is
PORT(a, b, ci : IN STD_LOGIC;
		  s, co : OUT STD_LOGIC);
END COMPONENT Full_Adder;

BEGIN

G1: FOR i IN 0 TO n-1 GENERATE
	G_start: IF i = 0 GENERATE
		fa : full_adder PORT MAP(a => num1(i), b => num2(i), ci => ci, co => CarryOuts(i), s => sum(i));
	END GENERATE;
	G_last: IF i = n-1 GENERATE
		fa : full_adder PORT MAP(a => num1(i), b => num2(i), ci => CarryOuts(i-1), co => carry_out, s => sum(i));
	END GENERATE;
	g_STANDARD: IF i > 0 AND i < n-1 GENERATE 
		fa : full_adder PORT MAP(a => num1(i), b => num2(i), ci => CarryOuts(i-1), co => CarryOuts(i), s => sum(i));
	END GENERATE;
END GENERATE;

END ARCHITECTURE summation;