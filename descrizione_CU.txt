L'unità di controllo(d'ora in avanti chiamata CU) è stata realizzata mediante l'implementazione di una macchina a stati e si occupa di gestire tutti i componenti del neurone

La CU deve svolgere 4 funzioni principali:
	attendere un impulso dall'sistema esterno per caricare i dati;
	caricare i dati all'interno della RAM A;
	eseguire le operazioni necessarie per calcolare i risultati e salvarli nella RAM B;
	attendere dal sistema esterno una conferma di avvenuta lettura e preparare il sistema per ripetere i punti precedenti;

Appena avviata la macchina il sistema si porta in uno stato di reset, durante il quale tutti i contatori, e celle di memoria vengono inizializzati a 0.

Il sistema esterno deve inviare un segnale di start per avviare la computazione e deve presentare un dato valido al colpo di clock immediatamente successivo al segnale di start.

La sequenza corretta deve essere del tipo START --> clock --> data_valid --> clock.

Un dato valido è un qualsiasi numero con segno rappresentabile su 8 bit, qualsiasi altro dato verrà comunque interpretato come numero con segno a 8 bit.

Dopo il primo dato valido il sistema esterno dovrà continuare a inviare sulla porta DATA_IN, i vari dati che il sistema dovrà elaborare.

Non appena la RAM A sarà piena, in questo caso è una memoria da 1024 parole ciascuna da 8 bit, la CU inizia a eseguire tutte le operazioni necessarie per l'elaborazione dei dati; durante tali operazione  avviene anche il salvataggio dei risultati all'interno della RAM B.

Non appena terminata l'elaborazione e quindi anche la RAM B è piena, anch'essa da 1024 parole da 8 bit ciascuna, la CU si mette in uno stato di idle, in cui sospende tutte le operazioni, e imposta la RAM B in sola lettura; inoltre cede il controllo degli indirizzi e dell'abilitazione di quest'ultima al sistema esterno, permettendogli così di leggere i dati.


Procedura di caricamento
	
	La CU si trova in uno stato di idle/reset e riceve il segnale di START;
	non appena accade ciò la CU abilita immediatamente la RAM A e la mette in modalità scrittura, inoltre abilita il contatore per gli indirizzi.
	
	La CU non esegue controlli sui dati in ingresso e continua a caricare dati nella RAM A finchè quest'ultima non sarà piena;
	questa condizione si verifica quando la CU riceve il segnale di terminal count (ossia quando il contatore è arrivato a contare 1023 = 1111111111).
	Non appena la RAM A è piena la CU inizia immediatamente dopo a eseguire la procedura di elaborazione dei dati.

	La sequenza degli stati della CU sarà clock(loading A) (end_count=0) --> clock(loading A) (end_count=1) --> clock (calcolo) (end_count=0)
	
	N.B.durante questo passaggio il contatore è andato in overflow e ripartirà da 0 alla prima fase di calcolo

Processo di calcolo
	
	La procedura di calcolo è suddivisa a sua volta in 3 stadi e necessità perciò di 3 clock per produrre il risultato di un singolo input.
	
	1) Si abilita la RAM A in lettura e si legge il dato presente all'indirizzo indicato dal contatore, vengono abilitati al campionamento i registri del datapath.
	
	2) Attendo che i sommatori completino le operazioni, non eseguo alcuna operazione, il datapath mi produce il risultato e mi comunica se vi è stato o meno overflow e se il numero è negativo o meno.
	
	3)La CU controlla se si è verificato overflow e se il numero è negativo, in base a tali informazioni decide quale numero memorizzare nella RAM B, tramite l'utilizzo del segnale sel, contemporaneamente abilita la RAM B alla scrittura all'indirizzo indicato dal contatore (che sarà identico all' indirizzo usato dalla RAM A per leggere il dato) e abilita quest'ultimo a contare (in preparazione per il passo 1).
	
	Questi 3 passi vengono ripetuti finchè il terminal count non sarà a 1 raggiunta tale condizione(ho esaurito gli i dati di A e ho riempito tutte le celle di B) la macchina si mette in uno stato di idle;

Stato di idle
	
	Appena entra nello stato di idle la CU porta il segnale DONE a 1, così facendo attiva dei MUX che scollegano il bus di indirizzi e il segnale di enable della RAM B dalla CU, ed espongono tali segnali all'esterno del neurone così che il sistema esterno può decidere come utilizzare i dati all'interno della RAM B.
	
	Il sistema rimarrà in questo stato di idle finchè non si instaurerà una procedura di handshake tra sistema esterno e CU.

Procedura di handshake
	
	La procedura di handshake è presente solo nello stato di idle della CU.
	
	Tale procedura prevede che durante tutta l'elaborazione il sistema esterno abbia mantenuto il segnale START a 1.
	
	Non appena la macchina entra nello stato di idle porta il segnale di DONE a 1;
	il sistema esterno riconoscendo che la CU ha terminato l'elaborazione e porta il segnale START a 0;
	la CU non appena riconosce che START è 0 riporta DONE a 0, la macchina è pronta per ricevere un nuovo segnale di START e ricomiciare.


